# RobotFramework 

## Installatie
Om RobotFramework te gebruiken heb je het volgende nodig:
1. Download en installeer Python 3+. **Zorg bij de installatie dat Python aan jouw PATH wordt toegevoegd**
2. Run de volgende commands: 
> pip install robotframework  
> pip install RESTinstance  
> pip install robotframework-seleniumlibrary  
3. Download de latest stable release van Chromedriver en zet deze in PATH. https://chromedriver.chromium.org/  
Als je stap 1 correct hebt gedaan kan je deze bijvoorbeeld in de C:/Python* folder zetten

## Gebruik
Gebruik RobotFramework via jouw terminal met de command:
> robot *TESTBESTANDLOCATIE*  

Dus bijvoorbeeld:
> robot Testcases/ApiTests.robot

De reports en eventuele screenshots zullen in de huidige folder worden gedumpt

## Documentatie
- RobotFramework: https://robotframework.org/
- RobotFramework BuiltIn keywords: https://robotframework.org/robotframework/latest/libraries/BuiltIn.html
- SeleniumLibrary: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html

## Visual Studio IDE support
1. Dowload de volgende extensies: *RobotF extension* en *Robot Framework Intellisense*
2. Ga naar File > Preferences > Settings > Robot Framework Intellisense
3. Open de volgende instelling:
4. Hier voeg je het volgende toe aan je settings.json:
> "rfLanguageServer.libraries":  
> [ "BuiltIn-3.0.4",  
> "SeleniumLibrary-3.3.1" ]
5. In je .robot files heb je nu autocomplete voor SeleniumLibrary en met de toestcombinatie **ALT + SHIFT + F** kan je de .robot bestanden formatten.
