*** Settings ***
Resource             ../Keywords/SeleniumKeywords.robot
Suite Teardown       Close All Browsers
Documentation        Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
...
...                  Kijk goed of er keywords zijn die je kan gebruiken in de Keywords/SeleniumKeywords.robot file

*** Variable ***
${SELENIUM_DELAY}    0
${TEST_URL}          http://cursusclient-${GROUP_NAME}-hco-kza-cursus.apps.prod.am5.hotcontainers.nl/cursussen
${GROUP_NAME}        harry_potter_house

*** Test Cases ***
Opdracht 0: Google Test
    # Als deze test werkt dan werkt RobotFramework goed. Verwijder deze als je begint.}
    Setup Browser                       https://www.google.com
    Set Selenium Speed                  ${SELENIUM_DELAY}
    Wait Until Page Contains Element    name=q                    timeout=5

Opdracht 1: Login in de cursusclient

Opdracht 2: Controleer of de pagina correct geladen is

Opdracht 3: Open het scherm om een nieuwe training aan te maken

Opdracht 4: Maak een nieuwe training aan

Opdracht 5: Meld je aan voor de training die je net hebt gemaakt

Opdracht 6: Controleer of de aangemaakte training in de lijst van trainingen terugkomt

Opdracht 7: Controleer de details pagina van de aangemaakte training

Opdracht 8: Verwijder de aangemaakte training

Opdracht 9: Test de logout functionaliteit
